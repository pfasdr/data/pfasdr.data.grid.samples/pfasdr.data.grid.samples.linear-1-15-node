from os.path import abspath
from pathlib import Path

ROOT_PATH: Path = Path(abspath(__file__)).parent.parent.parent.parent

DATA_PATH: Path = ROOT_PATH / 'data'

LINEAR_1_PATH: Path = DATA_PATH / 'length_1'
LINEAR_2_PATH: Path = DATA_PATH / 'length_2'
LINEAR_3_PATH: Path = DATA_PATH / 'length_3'
LINEAR_4_PATH: Path = DATA_PATH / 'length_4'
LINEAR_5_PATH: Path = DATA_PATH / 'length_5'
LINEAR_6_PATH: Path = DATA_PATH / 'length_6'
LINEAR_7_PATH: Path = DATA_PATH / 'length_7'
LINEAR_8_PATH: Path = DATA_PATH / 'length_8'
LINEAR_9_PATH: Path = DATA_PATH / 'length_9'
LINEAR_10_PATH: Path = DATA_PATH / 'length_10'
LINEAR_11_PATH: Path = DATA_PATH / 'length_11'
LINEAR_12_PATH: Path = DATA_PATH / 'length_12'
LINEAR_13_PATH: Path = DATA_PATH / 'length_13'
LINEAR_14_PATH: Path = DATA_PATH / 'length_14'
LINEAR_15_PATH: Path = DATA_PATH / 'length_15'
