import pytest

from pfasdr_grid_linear_1_15.grid_linear_1_15.util.paths import ROOT_PATH


@pytest.fixture(name='root_path')
def root_path_fixture():
    return ROOT_PATH
